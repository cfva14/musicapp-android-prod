package io.github.cfva14.musicapp.features.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import io.github.cfva14.musicapp.ui.MusicPlayerActivity;

/**
 * Created on 10/2/17.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, MusicPlayerActivity.class);
        startActivity(intent);
        finish();
    }

}
