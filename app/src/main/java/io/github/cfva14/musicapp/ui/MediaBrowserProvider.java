package io.github.cfva14.musicapp.ui;

/**
 * Created on 10/1/17.
 */

import android.support.v4.media.MediaBrowserCompat;

public interface MediaBrowserProvider {
    MediaBrowserCompat getMediaBrowser();
}
