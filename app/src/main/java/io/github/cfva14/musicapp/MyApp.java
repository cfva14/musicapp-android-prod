package io.github.cfva14.musicapp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.security.KeyStoreException;

import io.github.cfva14.musicapp.utils.RealmHelper;

/**
 * Created on 10/1/17.
 */

public class MyApp extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        // TODO: 10/6/17 Fisable for testing

        FirebaseCrash.setCrashCollectionEnabled(true);

        MultiDex.install(this);

        try {
            RealmHelper.initRealm(getApplicationContext());
        } catch (KeyStoreException e) {
            e.printStackTrace();
            FirebaseCrash.report(new Exception(e));
        }
    }
    public static Context getContext() {
        return mContext;
    }
}
