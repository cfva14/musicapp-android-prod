package io.github.cfva14.musicapp.model;

/**
 * Created on 10/1/17.
 */

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import io.github.cfva14.musicapp.MyApp;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.utils.LogHelper;
import io.github.cfva14.musicapp.utils.MediaIDHelper;

import static io.github.cfva14.musicapp.utils.MediaIDHelper.MEDIA_ID_MUSICS_BY_ALBUM;
import static io.github.cfva14.musicapp.utils.MediaIDHelper.MEDIA_ID_MUSICS_BY_ARTIST;
import static io.github.cfva14.musicapp.utils.MediaIDHelper.MEDIA_ID_MUSICS_BY_GENRE;
import static io.github.cfva14.musicapp.utils.MediaIDHelper.MEDIA_ID_MUSICS_BY_TRACK;
import static io.github.cfva14.musicapp.utils.MediaIDHelper.MEDIA_ID_ROOT;
import static io.github.cfva14.musicapp.utils.MediaIDHelper.OFFLINE;
import static io.github.cfva14.musicapp.utils.MediaIDHelper.createMediaID;

/**
 * Simple data provider for music tracks. The actual metadata source is delegated to a
 * MusicProviderSource defined by a constructor argument of this class.
 */
public class MusicProvider {

    private static final String TAG = LogHelper.makeLogTag(MusicProvider.class);

    private MusicProviderSource mSource;

    // Categorized caches for music track data:
    private ConcurrentMap<String, List<MediaMetadataCompat>> mMusicListByArtist;
    private ConcurrentMap<String, List<MediaMetadataCompat>> mMusicListByAlbum;
    private ConcurrentMap<String, List<MediaMetadataCompat>> mMusicListByGenre;
    private ConcurrentMap<String, List<String>> mAlbumListByArtist;
    private final ConcurrentMap<String, MutableMediaMetadata> mMusicListById;

    private ConcurrentMap<String, List<MediaMetadataCompat>> mOfflineMusicListByArtist;
    private ConcurrentMap<String, List<MediaMetadataCompat>> mOfflineMusicListByAlbum;
    private ConcurrentMap<String, List<MediaMetadataCompat>> mOfflineMusicListByGenre;
    private ConcurrentMap<String, List<String>> mOfflineAlbumListByArtist;
    private final ConcurrentMap<String, MutableMediaMetadata> mOfflineMusicListById;

    private final Set<String> mFavoriteTracks;

    enum State {
        NON_INITIALIZED, INITIALIZING, INITIALIZED
    }

    private volatile State mCurrentState = State.NON_INITIALIZED;

    public interface Callback {
        void onMusicCatalogReady(boolean success);
    }

    public MusicProvider() {
        this(new RemoteJSONSource());
    }
    public MusicProvider(MusicProviderSource source) {
        mSource = source;
        mMusicListByArtist = new ConcurrentHashMap<>();
        mMusicListByAlbum = new ConcurrentHashMap<>();
        mAlbumListByArtist = new ConcurrentHashMap<>();
        mMusicListByGenre = new ConcurrentHashMap<>();
        mMusicListById = new ConcurrentHashMap<>();

        mOfflineMusicListByArtist = new ConcurrentHashMap<>();
        mOfflineMusicListByAlbum = new ConcurrentHashMap<>();
        mOfflineAlbumListByArtist = new ConcurrentHashMap<>();
        mOfflineMusicListByGenre = new ConcurrentHashMap<>();
        mOfflineMusicListById = new ConcurrentHashMap<>();

        mFavoriteTracks = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
    }

    public long getOfflineMusicListSize() {
        return mOfflineMusicListById.size();
    }

    /**
     * Get an iterator over the list of artists
     *
     * @return artists
     */
    public Iterable<String> getArtists() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mMusicListByArtist.keySet();
    }

    /**
     * Get an iterator over the list of tracks
     *
     * @return artists
     */
    public Iterable<MediaMetadataCompat> getOfflineMusics() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }

        List<MediaMetadataCompat> tracks = new ArrayList<>();
        for (MutableMediaMetadata m : mOfflineMusicListById.values()) {
            tracks.add(m.metadata);
        }

        return tracks;
    }

    /**
     * Get an iterator over the list of tracks
     *
     * @return artists
     */
    public Iterable<MediaMetadataCompat> getMusics() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }

        List<MediaMetadataCompat> tracks = new ArrayList<>();
        for (MutableMediaMetadata m : mMusicListById.values()) {
            tracks.add(m.metadata);
        }

        return tracks;
    }

    /**
     * Get an iterator over the list of albums
     *
     * @return albums
     */
    public Iterable<String> getAlbums() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mMusicListByAlbum.keySet();
    }

    /**
     * Get an iterator over the list of offline artists
     *
     * @return artists
     */
    public Iterable<String> getOfflineArtists() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mOfflineMusicListByArtist.keySet();
    }

    /**
     * Get an iterator over the list of offline album
     *
     * @return albums
     */
    public Iterable<String> getOfflineAlbums() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mOfflineMusicListByAlbum.keySet();
    }

    /**
     * Get an iterator over the list of genres
     *
     * @return artists
     */
    public Iterable<String> getGenres() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mMusicListByGenre.keySet();
    }

    /**
     * Get an iterator over the list of offline genres
     *
     * @return artists
     */
    public Iterable<String> getOfflineGenres() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mOfflineMusicListByGenre.keySet();
    }

    /**
     * Get an iterator over the list of albums
     *
     * @return genres
     */
    public Iterable<String> getAlbumsByArtist(String artist) {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mAlbumListByArtist.get(artist);
    }

    /**
     * Get an iterator over the list of offline albums
     *
     * @return genres
     */
    public Iterable<String> getOfflineAlbumsByArtist(String artist) {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        return mOfflineAlbumListByArtist.get(artist);
    }

    /**
     * Get an iterator over a shuffled collection of all songs
     */
    public Iterable<MediaMetadataCompat> getShuffledMusic() {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        List<MediaMetadataCompat> shuffled = new ArrayList<>(mMusicListById.size());
        for (MutableMediaMetadata mutableMetadata: mMusicListById.values()) {
            shuffled.add(mutableMetadata.metadata);
        }
        Collections.shuffle(shuffled);
        return shuffled;
    }

    /**
     * Get music tracks of the given artist
     *
     */
    public Iterable<MediaMetadataCompat> getMusicsByArtist(String artist) {
        if (mCurrentState != State.INITIALIZED || !mMusicListByArtist.containsKey(artist)) {
            return Collections.emptyList();
        }
        return mMusicListByArtist.get(artist);
    }

    /**
     * Get music tracks of the given offline artist
     *
     */
    public Iterable<MediaMetadataCompat> getOfflineMusicsByArtist(String artist) {
        if (mCurrentState != State.INITIALIZED || !mOfflineMusicListByArtist.containsKey(artist)) {
            return Collections.emptyList();
        }
        return mOfflineMusicListByArtist.get(artist);
    }

    /**
     * Get music tracks of the given album
     *
     */
    public List<MediaMetadataCompat> getMusicsByAlbum(String album) {
        if (mCurrentState != State.INITIALIZED || !mMusicListByAlbum.containsKey(album)) {
            return Collections.emptyList();
        }
        return mMusicListByAlbum.get(album);
    }

    /**
     * Get music tracks of the given offline album
     *
     */
    public List<MediaMetadataCompat> getOfflineMusicsByAlbum(String album) {
        if (mCurrentState != State.INITIALIZED || !mOfflineMusicListByAlbum.containsKey(album)) {
            return Collections.emptyList();
        }
        return mOfflineMusicListByAlbum.get(album);
    }

    /**
     * Get music tracks of the given genre
     *
     */
    public List<MediaMetadataCompat> getMusicsByGenre(String genre) {
        if (mCurrentState != State.INITIALIZED || !mMusicListByGenre.containsKey(genre)) {
            return Collections.emptyList();
        }
        return mMusicListByGenre.get(genre);
    }

    /**
     * Get music tracks of the given offline genre
     *
     */
    public List<MediaMetadataCompat> getOfflineMusicsByGenre(String genre) {
        if (mCurrentState != State.INITIALIZED || !mOfflineMusicListByGenre.containsKey(genre)) {
            return Collections.emptyList();
        }
        return mOfflineMusicListByGenre.get(genre);
    }

    /**
     * Very basic implementation of a search that filter music tracks with title containing
     * the given query.
     *
     */
    public Iterable<MediaMetadataCompat> searchMusicBySongTitle(String query) {
        return searchMusic(MediaMetadataCompat.METADATA_KEY_TITLE, query);
    }

    /**
     * Very basic implementation of a search that filter music tracks with title containing
     * the given query.
     *
     */
    public Iterable<MediaMetadataCompat> searchOfflineMusicBySongTitle(String query) {
        return searchOfflineMusic(MediaMetadataCompat.METADATA_KEY_TITLE, query);
    }

    /**
     * Very basic implementation of a search that filter music tracks with album containing
     * the given query.
     *
     */
    public Iterable<MediaMetadataCompat> searchMusicByAlbum(String query) {
        return searchMusic(MediaMetadataCompat.METADATA_KEY_ALBUM, query);
    }

    /**
     * Very basic implementation of a search that filter music tracks with artist containing
     * the given query.
     *
     */
    public Iterable<MediaMetadataCompat> searchMusicByArtist(String query) {
        return searchMusic(MediaMetadataCompat.METADATA_KEY_ARTIST, query);
    }

    Iterable<MediaMetadataCompat> searchMusic(String metadataField, String query) {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        ArrayList<MediaMetadataCompat> result = new ArrayList<>();
        query = query.toLowerCase(Locale.US);
        for (MutableMediaMetadata track : mMusicListById.values()) {
            if (track.metadata.getString(metadataField).toLowerCase(Locale.US)
                    .contains(query)) {
                result.add(track.metadata);
            }
        }
        return result;
    }

    Iterable<MediaMetadataCompat> searchOfflineMusic(String metadataField, String query) {
        if (mCurrentState != State.INITIALIZED) {
            return Collections.emptyList();
        }
        ArrayList<MediaMetadataCompat> result = new ArrayList<>();
        query = query.toLowerCase(Locale.US);
        for (MutableMediaMetadata track : mOfflineMusicListById.values()) {
            if (track.metadata.getString(metadataField).toLowerCase(Locale.US)
                    .contains(query)) {
                result.add(track.metadata);
            }
        }
        return result;
    }


    /**
     * Return the MediaMetadataCompat for the given musicID.
     *
     * @param musicId The unique, non-hierarchical music ID.
     */
    public MediaMetadataCompat getMusic(String musicId) {
        return mMusicListById.containsKey(musicId) ? mMusicListById.get(musicId).metadata : null;
    }

    /**
     * Return the MediaMetadataCompat for the given musicID.
     *
     * @param musicId The unique, non-hierarchical music ID.
     */
    public MediaMetadataCompat getOfflineMusic(String musicId) {
        return mOfflineMusicListById.containsKey(musicId) ? mOfflineMusicListById.get(musicId).metadata : null;
    }

    public synchronized void updateMusicArt(String musicId, Bitmap albumArt, Bitmap icon) {
        MediaMetadataCompat metadata = getMusic(musicId);
        metadata = new MediaMetadataCompat.Builder(metadata)

                // set high resolution bitmap in METADATA_KEY_ALBUM_ART. This is used, for
                // example, on the lockscreen background when the media session is active.
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, albumArt)

                // set small version of the album art in the DISPLAY_ICON. This is used on
                // the MediaDescription and thus it should be small to be serialized if
                // necessary
                .putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, icon)

                .build();

        MutableMediaMetadata mutableMetadata = mMusicListById.get(musicId);
        if (mutableMetadata == null) {
            throw new IllegalStateException("Unexpected error: Inconsistent data structures in " +
                    "MusicProvider");
        }

        mutableMetadata.metadata = metadata;
    }

    public void setFavorite(String musicId, boolean favorite) {
        if (favorite) {
            mFavoriteTracks.add(musicId);
        } else {
            mFavoriteTracks.remove(musicId);
        }
    }

    public boolean isInitialized() {
        return mCurrentState == State.INITIALIZED;
    }

    public boolean isFavorite(String musicId) {
        return mFavoriteTracks.contains(musicId);
    }

    /**
     * Get the list of music tracks from a server and caches the track information
     * for future reference, keying tracks by musicId and grouping by artist.
     */
    public void retrieveMediaAsync(final Callback callback) {
        LogHelper.d(TAG, "retrieveMediaAsync called");
        if (mCurrentState == State.INITIALIZED) {
            if (callback != null) {
                // Nothing to do, execute callback immediately
                callback.onMusicCatalogReady(true);
            }
            return;
        }

        // Asynchronously load the music catalog in a separate thread
        new AsyncTask<Void, Void, State>() {
            @Override
            protected State doInBackground(Void... params) {
                retrieveMedia();
                return mCurrentState;
            }

            @Override
            protected void onPostExecute(State current) {
                if (callback != null) {
                    callback.onMusicCatalogReady(current == State.INITIALIZED);
                }
            }
        }.execute();
    }

    private synchronized void buildListsByArtist() {
        ConcurrentMap<String, List<MediaMetadataCompat>> newMusicListByArtist = new ConcurrentHashMap<>();

        for (MutableMediaMetadata m : mMusicListById.values()) {
            String artist = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ARTIST);
            List<MediaMetadataCompat> list = newMusicListByArtist.get(artist);
            if (list == null) {
                list = new ArrayList<>();
                newMusicListByArtist.put(artist, list);
            }
            list.add(m.metadata);
        }
        mMusicListByArtist = newMusicListByArtist;
    }

    private synchronized void buildOfflineListsByArtist() {
        ConcurrentMap<String, List<MediaMetadataCompat>> newOfflineMusicListByArtist = new ConcurrentHashMap<>();

        for (MutableMediaMetadata m : mOfflineMusicListById.values()) {
            String artist = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ARTIST);
            List<MediaMetadataCompat> list = newOfflineMusicListByArtist.get(artist);
            if (list == null) {
                list = new ArrayList<>();
                newOfflineMusicListByArtist.put(artist, list);
            }
            list.add(m.metadata);
        }
        mOfflineMusicListByArtist = newOfflineMusicListByArtist;
    }

    private synchronized void buildListsByAlbum() {
        ConcurrentMap<String, List<MediaMetadataCompat>> newMusicListByAlbum = new ConcurrentHashMap<>();

        for (MutableMediaMetadata m : mMusicListById.values()) {
            String album = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ALBUM);
            List<MediaMetadataCompat> list = newMusicListByAlbum.get(album);
            if (list == null) {
                list = new ArrayList<>();
                newMusicListByAlbum.put(album, list);
            }
            list.add(m.metadata);
        }
        mMusicListByAlbum = newMusicListByAlbum;
    }

    private synchronized void buildOfflineListsByAlbum() {
        ConcurrentMap<String, List<MediaMetadataCompat>> newOfflineMusicListByAlbum = new ConcurrentHashMap<>();

        for (MutableMediaMetadata m : mOfflineMusicListById.values()) {
            String album = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ALBUM);
            List<MediaMetadataCompat> list = newOfflineMusicListByAlbum.get(album);
            if (list == null) {
                list = new ArrayList<>();
                newOfflineMusicListByAlbum.put(album, list);
            }
            list.add(m.metadata);
        }
        mOfflineMusicListByAlbum = newOfflineMusicListByAlbum;
    }

    private synchronized void buildListsByGenre() {
        ConcurrentMap<String, List<MediaMetadataCompat>> newMusicListByGenre = new ConcurrentHashMap<>();

        for (MutableMediaMetadata m : mMusicListById.values()) {
            String genre = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_GENRE);
            List<MediaMetadataCompat> list = newMusicListByGenre.get(genre);
            if (list == null) {
                list = new ArrayList<>();
                newMusicListByGenre.put(genre, list);
            }
            list.add(m.metadata);
        }
        mMusicListByGenre = newMusicListByGenre;
    }

    private synchronized void buildOfflineListsByGenre() {
        ConcurrentMap<String, List<MediaMetadataCompat>> newOfflineMusicListByGenre = new ConcurrentHashMap<>();

        for (MutableMediaMetadata m : mOfflineMusicListById.values()) {
            String genre = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_GENRE);
            List<MediaMetadataCompat> list = newOfflineMusicListByGenre.get(genre);
            if (list == null) {
                list = new ArrayList<>();
                newOfflineMusicListByGenre.put(genre, list);
            }
            list.add(m.metadata);
        }
        mOfflineMusicListByGenre = newOfflineMusicListByGenre;
    }

    private synchronized void buildAlbumListByArtist() {
        ConcurrentMap<String, List<String>> newAlbumListByArtist = new ConcurrentHashMap<>();
        for (MutableMediaMetadata m : mMusicListById.values()) {
            String artist = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ARTIST);
            List<String> list = newAlbumListByArtist.get(artist);
            if (list == null) {
                list = new ArrayList<>();
                newAlbumListByArtist.put(artist, list);
            }
            if (!list.contains(m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ALBUM))) {
                list.add(m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ALBUM));
            }
        }
        mAlbumListByArtist = newAlbumListByArtist;
    }

    private synchronized void buildOfflineAlbumListByArtist() {
        ConcurrentMap<String, List<String>> newOfflineAlbumListByArtist = new ConcurrentHashMap<>();
        for (MutableMediaMetadata m : mOfflineMusicListById.values()) {
            String artist = m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ARTIST);
            List<String> list = newOfflineAlbumListByArtist.get(artist);
            if (list == null) {
                list = new ArrayList<>();
                newOfflineAlbumListByArtist.put(artist, list);
            }
            if (!list.contains(m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ALBUM))) {
                list.add(m.metadata.getString(MediaMetadataCompat.METADATA_KEY_ALBUM));
            }
        }
        mOfflineAlbumListByArtist = newOfflineAlbumListByArtist;
    }

    public void buildOfflineLists() {
        mOfflineMusicListByArtist.clear();
        mOfflineMusicListByAlbum.clear();
        mOfflineAlbumListByArtist.clear();
        mOfflineMusicListByGenre.clear();

        buildOfflineListsByArtist();
        buildOfflineListsByAlbum();
        buildOfflineAlbumListByArtist();
        buildOfflineListsByGenre();
    }

    public void addTrackToOfflinePlaylist(String musicId) {
        mOfflineMusicListById.put(musicId, mMusicListById.get(musicId));
        buildOfflineLists();
    }

    private synchronized void retrieveMedia() {
        try {
            if (mCurrentState == State.NON_INITIALIZED) {
                mCurrentState = State.INITIALIZING;

                Iterator<MediaMetadataCompat> tracks = mSource.iterator();
                while (tracks.hasNext()) {
                    MediaMetadataCompat item = tracks.next();
                    String musicId = item.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID);
                    mMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                    File file = new File(MyApp.getContext().getFilesDir(), musicId);
                    if (file.exists()) {
                        mOfflineMusicListById.put(musicId, new MutableMediaMetadata(musicId, item));
                    }
                }
                buildListsByArtist();
                buildListsByAlbum();
                buildAlbumListByArtist();
                buildListsByGenre();

                buildOfflineLists();
                mCurrentState = State.INITIALIZED;
            }
        } finally {
            if (mCurrentState != State.INITIALIZED) {
                // Something bad happened, so we reset state to NON_INITIALIZED to allow
                // retries (eg if the network connection is temporary unavailable)
                mCurrentState = State.NON_INITIALIZED;
            }
        }
    }


    public List<MediaBrowserCompat.MediaItem> getChildren(String mediaId, Resources resources) {
        List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList<>();
        if (mediaId.substring(0, 7).equals(OFFLINE)) {
            if (!MediaIDHelper.isBrowseable(mediaId)) {
                return mediaItems;
            }
            if ((OFFLINE + MEDIA_ID_ROOT).equals(mediaId)) {
                if (getOfflineMusicListSize() > 0) {
                    mediaItems.add(createBrowsableMediaItemForRoot(MEDIA_ID_MUSICS_BY_ARTIST, resources));
                    mediaItems.add(createBrowsableMediaItemForRoot(MEDIA_ID_MUSICS_BY_ALBUM, resources));
                    mediaItems.add(createBrowsableMediaItemForRoot(MEDIA_ID_MUSICS_BY_GENRE, resources));
                    mediaItems.add(createBrowsableMediaItemForRoot(MEDIA_ID_MUSICS_BY_TRACK, resources));
                }

            } else if ((OFFLINE + MEDIA_ID_MUSICS_BY_TRACK).equals(mediaId)) {
                for (MediaMetadataCompat metadata : getOfflineMusics()) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_TRACK, metadata, true));
                }

            } else if ((OFFLINE + MEDIA_ID_MUSICS_BY_ARTIST).equals(mediaId)) {
                Log.e(TAG, mediaId);
                for (String artist : getOfflineArtists()) {
                    mediaItems.add(createBrowsableMediaItemForArtist(artist, resources));
                }

            } else if ((OFFLINE + MEDIA_ID_MUSICS_BY_ALBUM).equals(mediaId)) {
                for (String album : getOfflineAlbums()) {
                    mediaItems.add(createBrowsableMediaItemForAlbum(album, resources));
                }

            } else if ((OFFLINE + MEDIA_ID_MUSICS_BY_GENRE).equals(mediaId)) {
                for (String genre : getOfflineGenres()) {
                    mediaItems.add(createBrowsableMediaItemForGenre(genre, resources));
                }

            } else if (mediaId.startsWith(OFFLINE + MEDIA_ID_MUSICS_BY_ARTIST)) {
                String artist = MediaIDHelper.getHierarchy(mediaId)[1];
                for (MediaMetadataCompat metadata : getOfflineMusicsByArtist(artist)) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_ARTIST, metadata, false));
                }

            } else if (mediaId.startsWith(OFFLINE + MEDIA_ID_MUSICS_BY_ALBUM)) {
                String album = MediaIDHelper.getHierarchy(mediaId)[1];
                for (MediaMetadataCompat metadata : getOfflineMusicsByAlbum(album)) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_ALBUM, metadata, false));
                }

            } else if (mediaId.startsWith(OFFLINE + MEDIA_ID_MUSICS_BY_ARTIST) && mediaId.endsWith(MEDIA_ID_MUSICS_BY_ALBUM)) {
                String[] split = mediaId.split("/");
                String artist = split[1];
                for (String album : getOfflineAlbumsByArtist(artist)) {
                    mediaItems.add(createBrowsableAlbumMediaItemForArtist(artist, album, resources));
                }

            } else if (mediaId.startsWith(OFFLINE + MEDIA_ID_MUSICS_BY_ARTIST) && mediaId.contains(MEDIA_ID_MUSICS_BY_ARTIST) && mediaId.contains(MEDIA_ID_MUSICS_BY_ALBUM)) {
                String album = MediaIDHelper.getHierarchy(mediaId)[3];
                for (MediaMetadataCompat metadata : getOfflineMusicsByAlbum(album)) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_ALBUM, metadata, true));
                }

            } else if (mediaId.startsWith(OFFLINE + MEDIA_ID_MUSICS_BY_GENRE)) {
                String genre = MediaIDHelper.getHierarchy(mediaId)[1];
                for (MediaMetadataCompat metadata : getOfflineMusicsByGenre(genre)) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_GENRE, metadata, true));
                }

            } else {
                LogHelper.w(TAG, "Skipping unmatched mediaId: ", mediaId);
            }
        } else {
            if (!MediaIDHelper.isBrowseable(mediaId)) {
                return mediaItems;
            }

            if (MEDIA_ID_ROOT.equals(mediaId)) {
                mediaItems.add(createBrowsableMediaItemForRoot(MEDIA_ID_MUSICS_BY_ARTIST, resources));
                mediaItems.add(createBrowsableMediaItemForRoot(MEDIA_ID_MUSICS_BY_ALBUM, resources));
                mediaItems.add(createBrowsableMediaItemForRoot(MEDIA_ID_MUSICS_BY_GENRE, resources));

            } else if (MEDIA_ID_MUSICS_BY_ARTIST.equals(mediaId)) {
                for (String artist : getArtists()) {
                    mediaItems.add(createBrowsableMediaItemForArtist(artist, resources));
                }
            } else if (MEDIA_ID_MUSICS_BY_ALBUM.equals(mediaId)) {
                for (String album : getAlbums()) {
                    mediaItems.add(createBrowsableMediaItemForAlbum(album, resources));
                }
            } else if (MEDIA_ID_MUSICS_BY_GENRE.equals(mediaId)) {
                Log.e(TAG, mediaId);
                for (String genre : getGenres()) {
                    mediaItems.add(createBrowsableMediaItemForGenre(genre, resources));
                }

            } else if (mediaId.startsWith(MEDIA_ID_MUSICS_BY_ARTIST)) {
                String artist = MediaIDHelper.getHierarchy(mediaId)[1];
                for (MediaMetadataCompat metadata : getMusicsByArtist(artist)) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_ARTIST, metadata, false));
                }

            } else if (mediaId.startsWith(MEDIA_ID_MUSICS_BY_ALBUM)) {
                String artist = MediaIDHelper.getHierarchy(mediaId)[1];
                for (MediaMetadataCompat metadata : getMusicsByAlbum(artist)) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_ALBUM, metadata, false));
                }

            } else if (mediaId.startsWith(MEDIA_ID_MUSICS_BY_ARTIST) && mediaId.endsWith(MEDIA_ID_MUSICS_BY_ALBUM)) {
                String[] split = mediaId.split("/");
                String artist = split[1];
                for (String album : getAlbumsByArtist(artist)) {
                    mediaItems.add(createBrowsableAlbumMediaItemForArtist(artist, album, resources));
                }

            } else if (mediaId.startsWith(MEDIA_ID_MUSICS_BY_ARTIST) && mediaId.contains(MEDIA_ID_MUSICS_BY_ARTIST) && mediaId.contains(MEDIA_ID_MUSICS_BY_ALBUM)) {
                String album = MediaIDHelper.getHierarchy(mediaId)[3];
                for (MediaMetadataCompat metadata : getMusicsByAlbum(album)) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_ALBUM, metadata, false));
                }

            } else if (mediaId.startsWith(MEDIA_ID_MUSICS_BY_GENRE)) {
                String genre = MediaIDHelper.getHierarchy(mediaId)[1];
                for (MediaMetadataCompat metadata : getMusicsByGenre(genre)) {
                    mediaItems.add(createMediaItem(MEDIA_ID_MUSICS_BY_GENRE, metadata, false));
                }

            }  else {
                LogHelper.w(TAG, "Skipping unmatched mediaId: ", mediaId);
            }
        }
        return mediaItems;
    }

    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForRoot(String mediaId, Resources resources) {
        String title = resources.getString(R.string.browse_unknown);
        String subTitle = resources.getString(R.string.browse_unknown_subtitle);;
        switch (mediaId) {
            case MEDIA_ID_MUSICS_BY_ARTIST:
                title = resources.getString(R.string.browse_artists);
                subTitle = resources.getString(R.string.browse_artist_subtitle);
                break;
            case MEDIA_ID_MUSICS_BY_ALBUM:
                title = resources.getString(R.string.browse_albums);
                subTitle = resources.getString(R.string.browse_album_subtitle);
                break;
            case MEDIA_ID_MUSICS_BY_GENRE:
                title = resources.getString(R.string.browse_genres);
                subTitle = resources.getString(R.string.browse_genre_subtitle);
                break;
            case MEDIA_ID_MUSICS_BY_TRACK:
                title = resources.getString(R.string.browse_tracks);
                subTitle = resources.getString(R.string.browse_track_subtitle);
                break;
        }

        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(mediaId)
                .setTitle(title)
                .setSubtitle(subTitle)
                .setIconUri(Uri.parse("android.resource://" + "com.example.android.uamp/drawable/ic_by_genre"))
                .build();
        return new MediaBrowserCompat.MediaItem(description, MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForArtist(String artist, Resources resources) {
        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, MEDIA_ID_MUSICS_BY_ARTIST, artist, MEDIA_ID_MUSICS_BY_ALBUM))
                .setTitle(artist)
                .setSubtitle(resources.getString(R.string.browse_musics_by_genre_subtitle, artist))
                .build();
        return new MediaBrowserCompat.MediaItem(description, MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForAlbum(String album, Resources resources) {
        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, MEDIA_ID_MUSICS_BY_ALBUM, album))
                .setTitle(album)
                .setSubtitle(resources.getString(R.string.browse_musics_by_genre_subtitle, album))
                .build();
        return new MediaBrowserCompat.MediaItem(description, MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    private MediaBrowserCompat.MediaItem createBrowsableMediaItemForGenre(String genre, Resources resources) {
        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, MEDIA_ID_MUSICS_BY_GENRE, genre))
                .setTitle(genre)
                .setSubtitle(resources.getString(R.string.browse_musics_by_genre_subtitle, genre))
                .build();
        return new MediaBrowserCompat.MediaItem(description, MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    private MediaBrowserCompat.MediaItem createBrowsableAlbumMediaItemForArtist(String artist, String album, Resources resources) {
        MediaDescriptionCompat description = new MediaDescriptionCompat.Builder()
                .setMediaId(createMediaID(null, MEDIA_ID_MUSICS_BY_ARTIST, artist, MEDIA_ID_MUSICS_BY_ALBUM, album))
                .setTitle(album)
                .setSubtitle(resources.getString(R.string.browse_musics_by_genre_subtitle, album))
                .build();
        return new MediaBrowserCompat.MediaItem(description, MediaBrowserCompat.MediaItem.FLAG_BROWSABLE);
    }

    private MediaBrowserCompat.MediaItem createMediaItem(String mediaId, MediaMetadataCompat metadata, boolean offline) {
        // Since mediaMetadata fields are immutable, we need to create a copy, so we
        // can set a hierarchy-aware mediaID. We will need to know the media hierarchy
        // when we get a onPlayFromMusicID call, so we can create the proper queue based
        // on where the music was selected from (by artist, by genre, random, etc)
        String value = metadata.getString(MediaMetadataCompat.METADATA_KEY_ARTIST);
        switch (mediaId) {
            case MEDIA_ID_MUSICS_BY_ARTIST:
                value = metadata.getString(MediaMetadataCompat.METADATA_KEY_ARTIST);
                break;
            case MEDIA_ID_MUSICS_BY_ALBUM:
                value = metadata.getString(MediaMetadataCompat.METADATA_KEY_ALBUM);
                break;
            case MEDIA_ID_MUSICS_BY_GENRE:
                value = metadata.getString(MediaMetadataCompat.METADATA_KEY_GENRE);
                break;
        }

        String hierarchyAwareMediaID = MediaIDHelper.createMediaID(metadata.getDescription().getMediaId(), mediaId, value);
        if (offline) {
            hierarchyAwareMediaID = MediaIDHelper.createMediaID(metadata.getDescription().getMediaId(), OFFLINE + mediaId, value);
        }
        MediaMetadataCompat copy = new MediaMetadataCompat.Builder(metadata)
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, hierarchyAwareMediaID)
                .build();
        return new MediaBrowserCompat.MediaItem(copy.getDescription(), MediaBrowserCompat.MediaItem.FLAG_PLAYABLE);
    }

}
