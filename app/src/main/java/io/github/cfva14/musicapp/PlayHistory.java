package io.github.cfva14.musicapp;

/**
 * Created on 10/12/17.
 */
public class PlayHistory {

    private String trackId;
    private long time;
    private String title;
    private String album;
    private String artist;

    public PlayHistory() {}

    public PlayHistory(String trackId, long time, String title, String album, String artist) {
        this.trackId = trackId;
        this.time = time;
        this.title = title;
        this.album = album;
        this.artist = artist;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
