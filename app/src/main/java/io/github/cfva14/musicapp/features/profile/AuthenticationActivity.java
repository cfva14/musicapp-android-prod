package io.github.cfva14.musicapp.features.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

import io.github.cfva14.musicapp.BuildConfig;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.ui.MusicPlayerActivity;
import io.github.cfva14.musicapp.utils.LogHelper;

public class AuthenticationActivity extends AppCompatActivity {

    private static final String TAG = LogHelper.makeLogTag(AuthenticationActivity.class);

    private static final int RC_SIGN_IN = 123;

    private DatabaseReference mUserRef = FirebaseDatabase.getInstance().getReference().child("user");
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            Bundle bundle = new Bundle();
            bundle.putString("STATUS", "LOGGED");
            bundle.putString("PROVIDER", "NULL");
            mFirebaseAnalytics.logEvent("LOGIN", bundle);
            startActivity(new Intent(AuthenticationActivity.this, MusicPlayerActivity.class));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("STATUS", "STARTED");
            bundle.putString("PROVIDER", "NULL");
            mFirebaseAnalytics.logEvent("LOGIN", bundle);
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                            .setAvailableProviders(
                                    Arrays.asList(
                                            new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.PHONE_VERIFICATION_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.TWITTER_PROVIDER).build()))
                            .build(),
                    RC_SIGN_IN);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            // Successfully signed in
            if (resultCode == RESULT_OK) {
                if (response != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("STATUS", "SUCCESS");
                    bundle.putString("PROVIDER", response.getProviderType());
                    mFirebaseAnalytics.logEvent("LOGIN", bundle);

                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    checkDatabase(auth.getCurrentUser(), response.getProviderType());
                    return;
                }
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    Bundle bundle = new Bundle();
                    bundle.putString("STATUS", "CANCELLED");
                    bundle.putString("PROVIDER", "NULL");
                    mFirebaseAnalytics.logEvent("LOGIN", bundle);
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    Bundle bundle = new Bundle();
                    bundle.putString("STATUS", "NO NETWORK");
                    bundle.putString("PROVIDER", "NULL");
                    mFirebaseAnalytics.logEvent("LOGIN", bundle);
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    Bundle bundle = new Bundle();
                    bundle.putString("STATUS", "UNKNOWN ERROR");
                    bundle.putString("PROVIDER", "NULL");
                    mFirebaseAnalytics.logEvent("LOGIN", bundle);
                    return;
                }
            }
            Bundle bundle = new Bundle();
            bundle.putString("STATUS", "UNKNOWN RESPONSE");
            bundle.putString("PROVIDER", "NULL");
            mFirebaseAnalytics.logEvent("LOGIN", bundle);
        }
    }

    private void checkDatabase(final FirebaseUser user, String provider) {
        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean newUser = true;
                for(DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (snap.getKey().equals(user.getUid())) {
                        newUser = false;
                    }
                }

                if (newUser) {
                    String id = user.getUid();
                    String email = "";
                    String displayName = "";
                    String photoUrl = "";
                    String username = user.getUid();
                    String phoneNumber = "";

                    if (user.getEmail() != null) {
                        email = user.getEmail();
                    }
                    if (user.getDisplayName() != null) {
                        displayName = user.getDisplayName();
                    }
                    if (user.getPhoneNumber() != null) {
                        phoneNumber = user.getPhoneNumber();
                    }
                    if (user.getPhotoUrl() != null) {
                        photoUrl = user.getPhotoUrl().toString();
                    }

                    mUserRef.child(user.getUid()).setValue(new User(id, email, displayName, photoUrl, username, phoneNumber));
                    startActivity(new Intent(AuthenticationActivity.this, MusicPlayerActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(AuthenticationActivity.this, MusicPlayerActivity.class));
                    finish();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
